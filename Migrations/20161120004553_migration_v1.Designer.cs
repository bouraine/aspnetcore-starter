﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using BDD.Postgres.Models;

namespace aspcoreSPA1.Migrations
{
    [DbContext(typeof(KanbanContext))]
    [Migration("20161120004553_migration_v1")]
    partial class migration_v1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("BDD.Postgres.Models.KanbanList", b =>
                {
                    b.Property<int>("KanbanListId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("KanbanTabId");

                    b.Property<int>("Name");

                    b.HasKey("KanbanListId");

                    b.HasIndex("KanbanTabId");

                    b.ToTable("KanbanList");
                });

            modelBuilder.Entity("BDD.Postgres.Models.KanbanTab", b =>
                {
                    b.Property<int>("KanbanTabId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("KanbanTabId");

                    b.ToTable("KanbanTab");
                });

            modelBuilder.Entity("BDD.Postgres.Models.KanbanTask", b =>
                {
                    b.Property<int>("KanbanTaskId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("KanbanListId");

                    b.Property<string>("Text")
                        .IsRequired();

                    b.HasKey("KanbanTaskId");

                    b.HasIndex("KanbanListId");

                    b.ToTable("KanbanTask");
                });

            modelBuilder.Entity("BDD.Postgres.Models.KanbanList", b =>
                {
                    b.HasOne("BDD.Postgres.Models.KanbanTab", "KanbanTab")
                        .WithMany("ListTaskCollection")
                        .HasForeignKey("KanbanTabId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BDD.Postgres.Models.KanbanTask", b =>
                {
                    b.HasOne("BDD.Postgres.Models.KanbanList", "KanbanList")
                        .WithMany("TaskCollection")
                        .HasForeignKey("KanbanListId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
