﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace aspcoreSPA1.Migrations
{
    public partial class migration_v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KanbanTab",
                columns: table => new
                {
                    KanbanTabId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KanbanTab", x => x.KanbanTabId);
                });

            migrationBuilder.CreateTable(
                name: "KanbanList",
                columns: table => new
                {
                    KanbanListId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    KanbanTabId = table.Column<int>(nullable: false),
                    Name = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KanbanList", x => x.KanbanListId);
                    table.ForeignKey(
                        name: "FK_KanbanList_KanbanTab_KanbanTabId",
                        column: x => x.KanbanTabId,
                        principalTable: "KanbanTab",
                        principalColumn: "KanbanTabId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "KanbanTask",
                columns: table => new
                {
                    KanbanTaskId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    KanbanListId = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KanbanTask", x => x.KanbanTaskId);
                    table.ForeignKey(
                        name: "FK_KanbanTask_KanbanList_KanbanListId",
                        column: x => x.KanbanListId,
                        principalTable: "KanbanList",
                        principalColumn: "KanbanListId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_KanbanList_KanbanTabId",
                table: "KanbanList",
                column: "KanbanTabId");

            migrationBuilder.CreateIndex(
                name: "IX_KanbanTask_KanbanListId",
                table: "KanbanTask",
                column: "KanbanListId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KanbanTask");

            migrationBuilder.DropTable(
                name: "KanbanList");

            migrationBuilder.DropTable(
                name: "KanbanTab");
        }
    }
}
