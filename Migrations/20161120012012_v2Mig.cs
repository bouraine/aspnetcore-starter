﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace aspcoreSPA1.Migrations
{
    public partial class v2Mig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "KanbanList",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Name",
                table: "KanbanList",
                nullable: false);
        }
    }
}
