using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace BDD.Postgres.Models
{
    [Table("KanbanTab")]
    public class KanbanTab{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KanbanTabId { get; set; }
        public string Name { get; set; }
        public List<KanbanList> ListTaskCollection { get; set; }
    }

    [Table("KanbanList")]
    public class KanbanList{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KanbanListId { get; set; }
        public string Name { get; set; }
        public int KanbanTabId { get; set; }
        [ForeignKey("KanbanTabId")]
        public KanbanTab KanbanTab { get; set; }
        public List<KanbanTask> TaskCollection { get; set; }
    }

    [Table("KanbanTask")]
    public class KanbanTask{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KanbanTaskId { get; set; }
        [Required] 
        public string Text { get; set; }
        public int KanbanListId { get; set; }
        [ForeignKey("KanbanListId")]
        public KanbanList KanbanList { get; set; }
    }
    
    public class KanbanContext : DbContext{
        public KanbanContext(DbContextOptions<KanbanContext> options):base(options){
            
        }
        public DbSet<KanbanTab> KanbanTabs {get;set;}
        public DbSet<KanbanList> KanbanLists{get;set;}
        public DbSet<KanbanTask> KanbanTasks{ get; set; }
    }
}