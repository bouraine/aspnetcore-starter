Créer un projet de zéro avec les (hot) technologies suivantes : 

1. **Aspnetcore** : la nouvelle version de Asp.net réécrite de zéro par Microsoft pour fonctionner sur Linux, Mac et  Windows bien sûr
1.  **Angular2**   : la nouvelle version d'Angular issue d'une collaboration entre Microsoft et Google 
1. **Webpack** : module bundler pour gérer (bundle, minify, uglify ...) les ressources (js, ts, css, less, sass, img, ...)  d'un site web
1.  **Aspnet identity** : gestionnnaire d'authentification développé par Microsoft 
1.  **Mongodb** : base de données nosql.

>  Passant aux choses sérieuses

**Installation aspnetcore + angular2 + webpack**

*  Installer dotnet core : https://www.microsoft.com/net/download/core

*  Installer nodejs : https://nodejs.org/en/

*  Installer npm :  npm install npm@latest -g

* Installer webpack :  npm install webpack -g

*  Installer Yeoman : npm install -g yo -> permet de générer des projet 

* Installer aspnetcore generator :   npm install -g generator-aspnetcore-angular2

* Générer le projet aspnetcore + angular2 + webpack  : npm install -g yo generator-aspnetcore-spa 

*  Créer un nouveau dossier "FirstAspnetcoreProject" : mkdir FirstAspnetcoreProject && cd FirstAspnetcoreProject 

* yo aspnetcore-spa -> pour créer le projet

* suivre les instructions (en choisissant angular2)

*  Lancer le projet : dotnet run 

>  Si jamais ça ne marche pas. Voici quelques commandes pour réinstaller les packages et régénérer le projet : 

* dotnet restore : restaurer les package asp
* npm install : restaurer les packes npm
* webpack : compiler les ressources "client" -> code utilisateurs
* webpack --config webpack.config.vendor.js : compiler les ressources "vendor"  -> code des libs externes
* Configurer l'environnement de dév : SET ASPNETCORE_ENVIRONMENT=Development
* dotnet build : générer les dll dotnetcore
* dotnet run : lancer le projet 
* dotnet --version ou npm -v ou node --version ou webpack -v : pour connaître les versions des différentes installations

> Liens utiles : 
* http://blog.stevensanderson.com/2016/05/02/angular2-react-knockout-apps-on-aspnet-core/
* https://docs.microsoft.com/en-us/aspnet/core/


**Mettre en place Aspnetcore Identity avec Mongodb :**

**Coming soon **


 