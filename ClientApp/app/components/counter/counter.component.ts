import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'counter',
    templateUrl: './counter.component.html'
})
export class CounterComponent {
    public currentCount = 0;
    
    
    constructor(http:Http){
        
    }
    
    public incrementCounter() {
        this.currentCount++;
    }

    public decrementCounter(){
        this.currentCount = this.currentCount-2;
    }
}
