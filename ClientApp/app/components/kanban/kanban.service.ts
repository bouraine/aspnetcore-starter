import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

@Injectable()
export class KanbanService {

    private urlBase = "/api/Kanban";
    private tasks:Array<any>;

    constructor(private http : Http) {}
    
    getTasks():any{
        return this.http.get('/api/Kanban/tasks');
    }
    
    addTask(name :string ):any{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.urlBase + "/add", {name});
    }
}