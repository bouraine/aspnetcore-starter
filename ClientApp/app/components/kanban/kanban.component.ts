import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import {KanbanService} from './Kanban.service';

@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.css'],
  providers: [KanbanService]
})

export class KanbanComponent implements OnInit {
  public tasks:any;
  
  constructor(private http : Http, private _KanbanService : KanbanService) { 
    this.getKtasks();
  }

  private getKtasks(){
    var resp = this._KanbanService.getTasks();
    resp.subscribe(
          result => {
            this.tasks = result.json();
          },
          err => {
              console.log(err);
          },
          () =>  {
              console.log('getTasks => ok');
          }
    );
  }

  ngOnInit() {
  }

}