
using System.Collections.Generic;
using BDD.Postgres.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;

[Route("api/[controller]")]
public class KanbanController:Controller
{
    private readonly KanbanContext _context;
    public KanbanController(KanbanContext context){
        _context = context;
    }
    
    [HttpGet("tasks")]
    public List<KanbanTask> getKTasks(){
        var tasks = _context.KanbanTasks.ToList();
        return tasks;
    }

    [HttpGet("tabs")]
    public List<KanbanTask> getKTabs(){
        var tasks = _context.KanbanTasks.ToList();
        return tasks;
    }
    
    [HttpGet("lists")]
    public List<KanbanTask> getKLists(){
        var tasks = _context.KanbanTasks.OrderBy(o=>o.KanbanList.KanbanTabId).ToList();
        return tasks;
    }
    
    [HttpPost("tabs/add")]
    public void AddKTab(string name){
        if(string.IsNullOrEmpty(name)) throw new ArgumentNullException("mauvais paramètres");
        var tab = new KanbanTab();
        tab.Name = name;
        _context.KanbanTabs.Add(tab);
        _context.SaveChanges();
    }

    [HttpPost("lists/add")]
    public void AddKList(string name, int idTab){
        if(string.IsNullOrEmpty(name) || idTab<1) throw new ArgumentNullException("mauvais paramètres");
        var list = new KanbanList();
        list.Name = name;
        list.KanbanTabId = idTab;
        _context.KanbanLists.Add(list);
        _context.SaveChanges();
    }

    [HttpPost("tasks/add")]
    public void AddKTask(string text, int idList){
        if(string.IsNullOrEmpty(text) || idList<1) throw new ArgumentNullException("mauvais paramètres");
        var task = new KanbanTask();
        task.KanbanListId = idList;
        task.Text = text;
    }

    public void AddKTask(string ptab, string plist, string ptask){
       
       var idTab = _context.KanbanTabs.FirstOrDefault(t=>t.Name.Equals(ptab));
       if(idTab==null){
           
       }

        //_context.KanbanTasks
        var tab = new KanbanTab();
        tab.Name = "tab1";
        _context.KanbanTabs.Add(tab);
        _context.SaveChanges();
        
        var list = new KanbanList();
        list.Name  = "list1";
        list.KanbanTabId = tab.KanbanTabId;
        _context.KanbanLists.Add(list);
        _context.SaveChanges();

        var tach = new KanbanTask();
        tach.Text = "first task";
        tach.KanbanListId = list.KanbanListId;
        _context.KanbanTasks.Add(tach);
        _context.SaveChanges();
    }
    
}