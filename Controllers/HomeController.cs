using BDD.Postgres.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspcoreSPA1.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly KanbanContext _context;
        
        public HomeController(KanbanContext context){
            _context = context;
        }

        public IActionResult Index()
        {
            ViewData["Description"] = "My first aspnetcore/angular2 App";            
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
